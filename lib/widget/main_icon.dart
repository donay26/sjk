import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MainIconWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: 1,
      duration: Duration(seconds: 1),
      child: SvgPicture.asset(
        "assets/carpool.svg",
        width: 100,
        height: 100,
      ),
    );
  }

}