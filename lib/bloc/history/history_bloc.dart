
import 'package:rxdart/rxdart.dart';
import 'package:sjk/model/history_model.dart';
import '../../core/repository.dart';

class HistoryBloc {
  final _repository = Repository();
  final _history = PublishSubject<HistoryModel>();

  Observable<HistoryModel> get allHistory => _history.stream;

  fetchAllMovies(String userId, String userName) async {
    HistoryModel itemModel = await _repository.getAllHistory(userId, userName);
    _history.sink.add(itemModel);
  }

  dispose() {
    _history.close();
  }
}

final historyBloc = HistoryBloc();