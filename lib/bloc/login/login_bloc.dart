
import 'dart:async';
 import 'package:rxdart/rxdart.dart';
import 'package:sjk/core/validators.dart';
import 'package:sjk/model/user_model.dart';
import '../../core/repository.dart';


class LoginBloc extends Object with Validators {
  final _repository = Repository();
  final _username = BehaviorSubject<String>();
  final _password = BehaviorSubject<String>();

  Stream<String> get username => _username.stream.transform(validateUsername);

  Stream<String> get password =>
      _password.stream.transform(validatePassword);

  Stream<bool> get submitValid  =>
      Observable.combineLatest2(username, password, (u,p) => true);

  Function(String) get changeUsername => _username.sink.add;

  Function(String) get changePassword => _password.sink.add;




  Future<UserModel> submit() {
    return _repository.postLogin(_username.value, _password.value);
  }


  void dispose() async {
    await _username.drain();
    _username.close();
    await _password.drain();
    _password.close();
  }

}
