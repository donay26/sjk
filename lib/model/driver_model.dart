class DriverModel {
  String name;
  String note;

  DriverModel({this.name, this.note});

  static List<DriverModel> driverList = [
    DriverModel(name: 'Dony', note: 'Ada tugas baru untuk kamu.'),
  ];
}
