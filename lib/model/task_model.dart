class TaskModel {
  int _id;
  String _email;
  String _first_name;
  String _last_name;
  String _avatar;

  TaskModel.fromMappedJson(Map<String, dynamic> parsedJson) {
    print(parsedJson['data'].length);
    _id = parsedJson['data']['id'];
    _email = parsedJson['data']['email'];
    _first_name = parsedJson['data']['first_name'];
    _last_name = parsedJson['data']['last_name'];
    _avatar = parsedJson['data']['avatar'];
  }

  int get id => _id;
  String get email => _email;
  String get first_name => _first_name;
  String get last_name => _last_name;
  String get avatar => _avatar;
}

class _Data {
  int _id;
  String _email;
  String _first_name;
  String _last_name;
  String _avatar;

  _Data(data) {
    _id = data['id'];
    _email = data['email'];
    _first_name = data['first_name'];
    _last_name = data['last_name'];
    _avatar = data['avatar'];
  }

  int get id => _id;
  String get email => _email;
  String get first_name => _first_name;
  String get last_name => _last_name;
  String get avatar => _avatar;
}
