import 'dart:convert';

import 'package:dio/dio.dart';

class UserModel {
  String _message;
  bool _status;
  _Data _data;
  String error;

  UserModel.fromJson(Map<String, dynamic> json) {
//    _message = json["message"];
    _status = json["status"];
    _data = _Data(json['data']);
  }

  String get message => _message;

  bool get status => _status;

  _Data get datas => _data;

  UserModel.withError(String errorValue)
      : error = errorValue;
}

//String userToJson(String username, String password) =>
//    json.encode(toJson(username, password));
FormData userToJson(String username, String password){
  FormData formData = new FormData.from({
    "username": username,
    "password": password,
    "imei": "12345",
    "version": "4.1.1",
    "fcm_token": "99",
    "fcm_token": "0",
    "ip_address": "0",
  });

  return formData;
}






Map<String, dynamic> toJson(String username, String password) => {
      "username": username,
      "password": password,
    };

class _Data {
  String _user_id;
  String _user_name;
  String _employee_id;
  String _user_type;

  _Data(data) {
    print(data['user_id']);
    _user_id = data['user_id'];
    _user_name = data['user_name'];
    _employee_id = data['employee_id'];
    _user_type = data['user_type'];
  }

  String get user_id => _user_id;

  String get user_name => _user_name;

  String get employee_id => _employee_id;

  String get user_type => _user_type;
}
