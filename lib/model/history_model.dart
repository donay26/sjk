
class HistoryModel {
  bool _status;
  String error;
  List<_History> _history = List();

  HistoryModel.fromJson(Map<String, dynamic> parsedJson) {
    print(parsedJson['data'].length);

 
    _status = parsedJson["status"]; 
    if(_status){
    List<_History> temp = [];
    for (int i = 0; i < parsedJson['data'].length; i++) {
      _History result = _History(parsedJson['data'][i]);
      temp.add(result);
    }
    _history = temp;
    }
  }

  List<_History> get results => _history;
  bool get status => _status;


  HistoryModel.withError(String errorValue)
      :_history = List(), error = errorValue;
}

class _History {
  String _sc_id;
  String _qr_url;
  String _username;
  String _alamat;
  String _suggestion;
  String _posting_date;
  String _prospect_nbr; 
  String _sfl_code;
  String _status_history;
  String _status_history_desc;

  _History(result) {
    _sc_id = result['sc_id'];
    _qr_url = result['qr_url'];
    _username = result['user_name'];
    _alamat = result['alamat'];
    _suggestion = result['suggestion'];
    _posting_date = result['posting_date'];
    _prospect_nbr = result['prospect_nbr'];
    _sfl_code = result['sfl_code'];
    _status_history = result['status'];
    _status_history_desc = result['status_desc'];
     
  }

  String get sc_id => _sc_id;

  String get qr_url => _qr_url;

  String get username => _username;

  String get alamat => _alamat;

  String get suggestion => _suggestion;

  String get posting_date => _posting_date;

  String get prospect_nbr => _prospect_nbr;

  String get sfl_code => _sfl_code;

  String get status_history => _status_history;

  String get status_history_desc => _status_history_desc;
   
}
