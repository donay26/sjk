import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:sjk/core/app_route.dart';
import 'package:sjk/page/splashscreen/splash.dart';

import 'bloc/login/login_bloc_provider.dart';



void main() async {
   runApp(MyApp());
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return LoginBlocProvider(
      child: CupertinoApp(
        builder: (context, child) => MediaQuery(
            data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
            child: child),
        title: 'CARPOOLING',
        theme: CupertinoThemeData(primaryColor: CupertinoColors.white),
        debugShowCheckedModeBanner: true,
        home: SplashScreen(),
        routes: AppRouter,
      ),
    );
  }
}
