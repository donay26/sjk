import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sjk/model/history_model.dart';
import 'package:sjk/page/detail_history/detail_history.dart';

class HistoryItem extends StatelessWidget {
  HistoryModel data;
  int index;

  HistoryItem(this.data, this.index);

  @override
  Widget build(BuildContext context) {
    return new Card(
      elevation: 8.0,
      margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
        child: makeListTile(data.results[index],context),
      ),
    );
  }

  ListTile makeListTile(result,context) => ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        leading: Container(
          padding: EdgeInsets.only(right: 12.0),
          decoration: BoxDecoration(
            border:
                Border(right: BorderSide(width: 1.0, color: Colors.white24)),
          ),
          child: Icon(
            Icons.drive_eta,
            color: Colors.white,
          ),
        ),
        title: Text(
          result.sc_id,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        subtitle: Row(
          children: <Widget>[
            Expanded(
                flex: 4,
                child: Text(result.posting_date,
                    style: TextStyle(color: Colors.white)))
          ],
        ),
        trailing:
            Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30.0),
        onTap: () {
          goToDetail(context,result);
        },
      );

  goToDetail(context,result){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => DetailHistory(result: result)),
    );
  }

}
