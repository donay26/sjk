import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sjk/model/history_model.dart';
import 'package:sjk/page/history/item/history_item.dart';
import '../../bloc/history/history_bloc.dart';

class HistoryScreen extends StatefulWidget {
  @override
  _HistoryScreenState createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {

  @override
  void initState() {
    super.initState();
    historyBloc.fetchAllMovies("bosaputra", "bosaputra");
  }

  @override
  void dispose() {
    historyBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      resizeToAvoidBottomInset: true,
      child: SafeArea(
        child: StreamBuilder(
          stream: historyBloc.allHistory,
          builder: (context, AsyncSnapshot<HistoryModel> snapshot) {
            if (snapshot.hasData) {
              return buildList(snapshot);
            } else if (snapshot.hasData) {
              return Text(snapshot.error.toString());
            }
            return Center(child: new CupertinoActivityIndicator(
              animating: true,
            ));
          },
        ),
      ),
    );
  }

  Widget buildList(AsyncSnapshot<HistoryModel> snapshot) {
    return ListView.builder(
        scrollDirection:Axis.vertical,
        shrinkWrap: true,
        itemBuilder: (context, index){
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: HistoryItem(snapshot.data,index),
        );
      },
      itemCount: snapshot.data.results.length,
    );
  }

}