import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sjk/model/driver_model.dart';
import 'package:sjk/model/task_model.dart';

class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}


class _HomeState extends State<Home> {
  String _homeScreenText = "Waiting for token...";
  String lastSelectedValue = "Waiting for token...";
  int _bottomNavBarSelectedIndex = 0;
  bool _newNotification = false;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

  List<DriverModel> driverModelData = DriverModel.driverList;


  @override
  void initState() {
    super.initState();

    var initializationSettingsAndroid = new AndroidInitializationSettings('@mipmap/ic_launcher');

    var initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidRecieveLocalNotification);

    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);

    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
        print('on message ${message}');
        // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
        displayNotification(message);
        // _showItemDialog(message);
        setState(() {
          _newNotification = true;
        });
      },
      onResume: (Map<String, dynamic> message) {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) {
        print('on launch $message');
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      print(token);
    });

  }





  @override
  void dispose() {
    super.dispose();
  }

  @override
  didPopRoute() {

  }


  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      backgroundColor: Color(0xFFE0E0E0),
      navigationBar: CupertinoNavigationBar(
        automaticallyImplyLeading: false,
        middle: Text("TASK"),
        trailing: GestureDetector(
          onTap: () {
            _onActionSheetPress(context);
          },
          child: Icon(
            CupertinoIcons.settings,
            color: CupertinoColors.black,
          ),
        ),
      ),
      child: SafeArea(
          child: !_newNotification
              ? EmptyTask() : GetTask(),
      ),
    );
  }

  Widget EmptyTask () {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Center (
          child : Container(
          padding: EdgeInsets.only(top: 8),
          child: Text(
            'Tidak ada tugas sementara ini!',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        ),
    Center (
    child : Container(
          padding: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top,
              left: 16,
              right: 16),
          child: Image.asset("assets/parking_car.png"),
          height: 250,
          width: 250,
        ),
        ),
        Center (
        child : Container(
          padding: EdgeInsets.only(top: 16),
          child: Text(
            "Setiap tugas yang anda terima\nakan ditayangkan disini. Semangat untuk hari ini\nsemoga hari ini adalah hari terbaik anda",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16,
            ),
          ),
          ),
        )
      ],
    );
  }

  Widget GetTask(){
    return Container (
      margin: EdgeInsets.all(10),
      child : Card(
      elevation: 10,
      child : Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(40.0),
          margin: EdgeInsets.only(top: 40.0),
          child: Text(
            'Hay, ${driverModelData[0].name}',
            style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.w500),
          ),
        ),
        Container (
          padding: EdgeInsets.all(40.0),
          child: TaskDetail(),
        ),
        Container (
          margin: EdgeInsets.only(top: 20.0),
          child : Row (
            children: <Widget>[
              Container (
                padding: EdgeInsets.only(left: 40.0,right: 0.0),
                width: MediaQuery.of(context).size.width * 0.7,
                child :Text(
                'Untuk tugas ini, kamu bisa mengambil uang saku ke Finance',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w300,
                  letterSpacing: 1,
                ),
                ),
              ),
              Container (
                margin: EdgeInsets.only(left: 20.0),
                decoration: new BoxDecoration(
                  color: Colors.white,
                  border: new Border.all(
                    color: Colors.black.withOpacity(0.5),
                    width: 15.0, // it's my slider variable, to change the size of the circle
                  ),
                  shape: BoxShape.circle,
                ),              ),
            ]),),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.05,
        ),
        submitButton(),
      ],
    ),
    ),
    );
  }

  Widget submitButton() {
    return Container(
      child: CupertinoButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/timeline');
                },
                child: Container(
                  height: 50.0,
                  alignment: FractionalOffset.center,
                  decoration: new BoxDecoration(
                    color: CupertinoColors.black,
                    borderRadius:
                    new BorderRadius.all(const Radius.circular(30.0)),
                  ),
                  child: Text(
                    "Checkout",
                    style: new TextStyle(
                      color: CupertinoColors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w300,
                      letterSpacing: 0.3,
                    ),
                  )
                )),
    );
  }


  Widget TaskDetail(){
    var fontName = "Roboto";

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          child: Row (
              children: <Widget>[
                Text(
                  'No.',
                  style: TextStyle(
                    fontSize: 18,
                    fontFamily: fontName,
                    fontWeight: FontWeight.w400,
                    letterSpacing: 1,
                  ),
                ),
                Text(
                  '123456',
                  style: TextStyle(
                    fontSize: 18,
                    fontFamily: fontName,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1.2,
                  ),
                ),
              ]),
        ),
        Container(
          child: Row (
              children: <Widget>[
                Text(
                  'Tanggal ',
                  style: TextStyle(
                    fontSize: 18,
                    fontFamily: fontName,
                    fontWeight: FontWeight.w400,
                    letterSpacing: 1,
                  ),
                ),
                Text(
                  '10 Sept 2019',
                  style: TextStyle(
                    fontSize: 18,
                    fontFamily: fontName,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1.2,
                  ),
                ),
              ]),
        ),
        Container(
          child: Row (
              children: <Widget>[
                Text(
                  'Jam Berangkat ',
                  style: TextStyle(
                    fontSize: 18,
                    fontFamily: fontName,
                    fontWeight: FontWeight.w400,
                    letterSpacing: 1,
                  ),
                ),
                Text(
                  '21:00',
                  style: TextStyle(
                    fontSize: 18,
                    fontFamily: fontName,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1.2,
                  ),
                ),
              ]),
        ),
        Container(
          child: Row (
              children: <Widget>[
                Text(
                  'Tujuan akhir ',
                  style: TextStyle(
                    fontSize: 18,
                    fontFamily: fontName,
                    fontWeight: FontWeight.w400,
                    letterSpacing: 1,
                  ),
                ),
                Text(
                  'Parung, Bogor',
                  style: TextStyle(
                    fontSize: 18,
                    fontFamily: fontName,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1.2,
                  ),
                ),
              ]),
        ),
        Container(
          child: Row (
              children: <Widget>[
                Text(
                  'Jumlah penumpang ',
                  style: TextStyle(
                    fontSize: 18,
                    fontFamily: fontName,
                    fontWeight: FontWeight.w400,
                    letterSpacing: 1,
                  ),
                ),
                Text(
                  '7 orang',
                  style: TextStyle(
                    fontSize: 18,
                    fontFamily: fontName,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1.2,
                  ),
                ),
              ]),
        ),
      ],
    );
  }

  void showDemoActionSheet({BuildContext context, Widget child}) {
    showCupertinoModalPopup<String>(
      context: context,
      builder: (BuildContext context) => child,
    ).then((String value) {
      if (value != null) {
        setState(() {
          lastSelectedValue = value;
        });
      }
    });
  }

  void _onActionSheetPress(BuildContext context) {
    showDemoActionSheet(
      context: context,
      child: CupertinoActionSheet(
        title: const Text('Menu'),
        message: const Text('Silahkan pilih menu dibawah ini'),
        actions: <Widget>[
          CupertinoActionSheetAction(
            child: const Text(
              'AKUN',
              style: TextStyle(
                  fontSize: 20.0, color: CupertinoColors.systemGrey),
            ),
            onPressed: () => Navigator.pop(context, 'Akun'),
          ),
          CupertinoActionSheetAction(
            child: const Text(
              'HISTORY',
              style: TextStyle(
                  fontSize: 20.0, color: CupertinoColors.systemGrey),
            ),
            onPressed: () => Navigator.pop(context, 'History'),
          ),
          CupertinoActionSheetAction(
            child: const Text(
              'EXIT',
              style: TextStyle(
                  fontSize: 20.0, color: CupertinoColors.systemGrey),
            ),
            onPressed: () => Navigator.pop(context, 'Exit'),
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          child: const Text(
            'CANCEL',
            style: TextStyle(fontSize: 20.0, color: CupertinoColors.systemGrey),
          ),
          isDefaultAction: true,
          onPressed: () => Navigator.pop(context, 'Cancel'),
        ),
      ),
    );
  }

  Future displayNotification(Map<String, dynamic> message) async{
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'channelid', 'flutterfcm', 'your channel description',
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0,
      message['notification']['title'],
      message['notification']['body'],
      platformChannelSpecifics,
      payload: 'hello',);
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payloads: ' + payload);
    }
    await Fluttertoast.showToast(
        msg: "Notification Clicked",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
        fontSize: 16.0
    );
    /*Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => new SecondScreen(payload)),
    );*/
  }

  Future onDidRecieveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) =>
      new CupertinoAlertDialog(
        title: new Text(title),
        content: new Text(body),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: new Text('Ok'),
            onPressed: () async {
              Navigator.of(context, rootNavigator: true).pop();
              await Fluttertoast.showToast(
                  msg: "Notification Clicked",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIos: 1,
                  backgroundColor: Colors.black54,
                  textColor: Colors.white,
                  fontSize: 16.0
              );
            },
          ),
        ],
      ),
    );
  }

}