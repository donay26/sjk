import 'package:flutter/cupertino.dart';


class AccountScreen extends StatelessWidget {


  Widget _buildFullName() {
    TextStyle _nameTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: CupertinoColors.black,
      fontSize: 28.0,
      fontWeight: FontWeight.w700,
    );

    return Text(
      "Boy Saputra",
      style: _nameTextStyle,
    );
  }
  Widget _buildStatus() {
    TextStyle _nameTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: CupertinoColors.black,
      fontSize: 18.0,
      fontWeight: FontWeight.w500,
    );

    return Text(
      "Driver -  MNC VISION",
      style: _nameTextStyle,
    );
  }

  Widget _buildStatusCar() {
    TextStyle _nameTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: CupertinoColors.black,
      fontSize: 18.0,
      fontWeight: FontWeight.w500,
    );

    return Text(
      "Daihatsu -  B 1234 MNC",
      style: _nameTextStyle,
    );
  }




  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const Icon(
              CupertinoIcons.profile_circled,
              size: 160.0,
              color: Color(0xFF646464),
            ),
            const Padding(padding: EdgeInsets.only(top: 18.0)),
            _buildFullName(),
            const Padding(padding: EdgeInsets.only(top: 5.0)),
            _buildStatus(),
            const Padding(padding: EdgeInsets.only(top: 5.0)),
            _buildStatusCar(),
            const Padding(padding: EdgeInsets.only(top: 20.0)),
            CupertinoButton(
              child: const Text('Sign Out'),
              color: CupertinoColors.black,
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}