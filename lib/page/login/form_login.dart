import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sjk/core/app_constant_string.dart';
import '../../bloc/login/login_bloc_provider.dart';

class FormLogin extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _FormLoginState();
}

class _FormLoginState extends State<FormLogin> {
  LoginBloc _bloc;
  bool isLoading = false;
  bool _isButtonDisabled = false;
  TextEditingController _usernameField = TextEditingController();
  TextEditingController _passwordField = TextEditingController();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _bloc = LoginBlocProvider.of(context);
  }

  @override
  void initState() {
    _isButtonDisabled = false;
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  Widget register() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("Belum punya akun ?"),
          GestureDetector(
            onTap: () => Navigator.pushNamed(context, '/register'),
            child: Padding(
              padding: new EdgeInsets.all(10.0),
              child: Text(
                "DAFTAR",
                style: new TextStyle(
                  decoration: TextDecoration.underline,
                  color: CupertinoColors.black,
                  fontSize: 15.0,
                  fontWeight: FontWeight.w300,
                  letterSpacing: 0.3,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
            emailForm(),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
            passForm(),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
            descText(),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.25,
            ),
            submitButton(),
          ],
        ));
  }

  Widget descText() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Text(
          "Lupa Pasword ?",
          textAlign: TextAlign.end,
          style: TextStyle(color: CupertinoColors.black, fontSize: 15.0),
        )
      ],
    );
  }

  Widget descTextDialog(String text) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
            width: 150,
            child: Text(
              text.toString(),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              textDirection: TextDirection.ltr,
              textAlign: TextAlign.center,
              style: TextStyle(color: CupertinoColors.black, fontSize: 15.0),
            )),
      ],
    );
  }

  Widget emailForm() {
    return StreamBuilder(
        stream: _bloc.username,
        builder: (context, snapshot) {
          return CupertinoTextField(
            onChanged: _bloc.changeUsername,
            cursorColor: CupertinoColors.black,
            controller: _usernameField,
            keyboardType: TextInputType.text,
            prefix: Padding(
              padding: EdgeInsets.all(5.0),
              child: Icon(
                CupertinoIcons.person_solid,
                color: CupertinoColors.lightBackgroundGray,
                size: 28.0,
              ),
            ),
            decoration: BoxDecoration(
              border: Border.all(color: CupertinoColors.lightBackgroundGray),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
            placeholder: "Masukan Username Anda",
            autocorrect: false,
            clearButtonMode: OverlayVisibilityMode.editing,
          );
        });
  }

  Widget passForm() {
    return StreamBuilder(
        stream: _bloc.password,
        builder: (context, snapshot) {
          return CupertinoTextField(
            onChanged: _bloc.changePassword,
            obscureText: true,
            cursorColor: CupertinoColors.black,
            controller: _passwordField,
            prefix: Padding(
              padding: EdgeInsets.all(5.0),
              child: Icon(
                CupertinoIcons.padlock_solid,
                color: CupertinoColors.lightBackgroundGray,
                size: 28.0,
              ),
            ),
            decoration: BoxDecoration(
                border: Border.all(color: CupertinoColors.lightBackgroundGray),
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
            placeholder: "Masukan Password Anda",
            autocorrect: false,
            clearButtonMode: OverlayVisibilityMode.editing,
          );
        });
  }

  void submit(LoginBloc userBloc) async {
    setState(() {
      isLoading = true;
      _isButtonDisabled = true;
    });

    _bloc.submit().then((value) {
      setState(() {
        isLoading = false;
        _isButtonDisabled = false;
      });

      if (value.status) {
        print(value.datas.toString());
        Navigator.pushNamed(context, '/home');
      } else {
        print(value.error);
      }
    });

  }

  /*void _showErrorDialog(String text) {
    CupertinoAlertDialog(
      title: Text("Terjadi Kesalahan"),
      content: Text(text),
      actions: <Widget>[
        CupertinoDialogAction(
          child: Text("Oke"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }*/

  void showDemoDialog({BuildContext context, String text}) {
    showCupertinoDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: const Text('Terjadi kesalahan!'),
          content: Column(
            children: <Widget>[
              Divider(
                color: CupertinoColors.inactiveGray,
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              descTextDialog(text),
            ],
          ),
          actions: <Widget>[
            CupertinoDialogAction(
              child: const Text('Close'),
              isDefaultAction: true,
              onPressed: () {
                Navigator.pop(context, 'Close');
              },
            ),
          ],
        );
      },
    );
  }

  void _showDialog(var response) {
    showCupertinoDialog(
      context: context,
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text("Terjadi Kesalahan"),
          content: Text(response["message"]),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text("Oke"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget submitButton() {
    return Container(
      child: StreamBuilder(
          stream: _bloc.submitValid,
          builder: (context, snapshot) {
            return CupertinoButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/home');
/*
                  if (_usernameField.text.isEmpty) {
                    showDemoDialog(
                        context: context,
                        text: AppConstantString.usernameErrorHint);
                  } else if (_passwordField.text.isEmpty) {
                    showDemoDialog(
                        context: context,
                        text: AppConstantString.passwordErrorHint);
                  } else {
                    if (snapshot.hasData) {
                      _isButtonDisabled ? null : submit(_bloc);
                    } else {
                      showDemoDialog(context: context, text: snapshot.error);
                    }
                  }
*/
                },
                child: Container(
                  height: 50.0,
                  alignment: FractionalOffset.center,
                  decoration: new BoxDecoration(
                    color: CupertinoColors.black,
                    borderRadius:
                    new BorderRadius.all(const Radius.circular(30.0)),
                  ),
                  child: !isLoading
                      ? new Text(
                    AppConstantString.submit,
                    style: new TextStyle(
                      color: CupertinoColors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w300,
                      letterSpacing: 0.3,
                    ),
                  )
                      : new CupertinoActivityIndicator(
                    animating: true,
                  ),
                ));
          }),
    );
  }
}
