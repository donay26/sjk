import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sjk/widget/main_icon.dart';
import '../../bloc/login/login_bloc_provider.dart';
import 'package:sjk/page/login/form_login.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  Widget divider() {
    return new Row(
      children: <Widget>[
        new Flexible(
          child: new Center(
            child: new Container(
              margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
              height: 2.0,
              color: CupertinoColors.black,
            ),
          ),
        ),
        new Padding(
          padding: const EdgeInsets.only(left: 5.0, right: 5.0),
          child: Text(
            "LOGIN",
            style: TextStyle(fontSize: 20.0),
          ),
        ),
        new Flexible(
          child: new Center(
            child: new Container(
              margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
              height: 2.0,
              color: CupertinoColors.black,
            ),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    double padding = MediaQuery.of(context).size.width * 0.1;
//    return LoginBlocProvider(
    return CupertinoPageScaffold(
      resizeToAvoidBottomInset: true,
      child: SafeArea(
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(horizontal: padding.toDouble()),
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
              ),
              MainIconWidget(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
              ),
              divider(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
              ),
              FormLogin(),
            ],
          ),
        ),
      ),
//      ),
    );
  }

//  @override
//  Widget build(BuildContext context) {
//    double padding = MediaQuery.of(context).size.width * 0.1;
//    return UserProvider(
//      child: Scaffold(
//        resizeToAvoidBottomPadding: true,
//        body: Container(
//          decoration: BoxDecoration(
////          image: DecorationImage(
////              image: AssetImage("assets/login-bg.png"), fit: BoxFit.fitWidth),
//              ),
//          child: Container(
//            width: MediaQuery.of(context).size.width,
//            padding: EdgeInsets.symmetric(horizontal: padding.toDouble()),
//            child: ListView(
//              children: <Widget>[
//                SizedBox(
//                  height: MediaQuery.of(context).size.height * 0.05,
//                ),
//                mainLogo,
//                SizedBox(
//                  height: MediaQuery.of(context).size.height * 0.02,
//                ),
//                FormLogin(),
//              ],
//            ),
//          ),
//        ),
//      ),
//    );
//  }
}
