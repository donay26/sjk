import 'dart:async';

import 'app_constant_string.dart';

class Validators {
  final validateUsername = StreamTransformer<String, String>.fromHandlers(
      handleData: (username, sink) {
        print(username.length);
    if (username.length > 3) {
      sink.add(username);
    } else {
      sink.addError(AppConstantString.usernameValidateCountMessage);
    }
  });

  final validatePassword = StreamTransformer<String, String>.fromHandlers(
      handleData: (password, sink) {
    if (password.length > 4) {
      sink.add(password);
    } else {
      sink.addError(AppConstantString.passwordValidateMessage);
    }
  });
}
