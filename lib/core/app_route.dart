
import 'package:sjk/page/detail_history/detail_history.dart';
import 'package:sjk/page/home/home.dart';
import 'package:sjk/page/login/login.dart';
import 'package:sjk/page/register/register.dart';

final AppRouter = {
   '/login': (context) => Login(),
  '/register': (context) => Register(),
  '/home': (context) => Home(),
};