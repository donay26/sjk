
import 'api_provider.dart';
import 'package:sjk/model/user_model.dart';
import 'package:sjk/model/history_model.dart';

class Repository {
  final apiProvider = ApiProvider();

  Future<UserModel>  postLogin(users, password) => apiProvider.postUsers(users, password);

  Future<HistoryModel>  getAllHistory(userId,userName) => apiProvider.getAllHistory(userId, userName);

}