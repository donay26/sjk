class AppConstantString {
   static const String emailField = "email";
   static const String emailValidateMessage = "Enter a valid email";
  static const String emailValidateCountMessage = "Email must be at least 4 characters";
  static const String usernameValidateCountMessage = "Username must be at least 3 characters";
  static const String passwordValidateMessage = "Password must be at least 4 characters";
  static const String passwordErrorHint = "Enter a Password";
  static const String usernameErrorHint = "Enter a Username";
  static const String submit = "Submit";


}