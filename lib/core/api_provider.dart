import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:sjk/core/app_constant_url.dart';
import '../model/user_model.dart';
import '../model/history_model.dart';



class ApiProvider {
    Dio _dio = new Dio();


Future<UserModel> postUsers(String users,String password) async {
    try {
      String url = AppConstantUrl.LoginLink;
      print(url);
      Response response = await _dio.post(url, data: userToJson(users, password));
      print(response.data);
      return UserModel.fromJson(json.decode(response.data));
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return UserModel.withError("$error");
    }
  }


Future<HistoryModel> getAllHistory(String userId,String userName) async {
    try {
      String url = AppConstantUrl.HistoryLink+userName;
      print(url);
      Response response = await _dio.get(url);
      return HistoryModel.fromJson(json.decode(response.data));
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return HistoryModel.withError("$error");
    }
  }
}